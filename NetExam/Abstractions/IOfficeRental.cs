﻿namespace NetExam.Abstractions
{
    using System.Collections.Generic;
    using NetExam.Dto;

    public interface IOfficeRental
    {
        void AddLocation(LocationSpecs locationSpecs);

        IEnumerable<LocationSpecs> GetLocations();

        void AddOffice(OfficeSpecs officeSpecs);

        IEnumerable<OfficeSpecs> GetOffices(string locationName);

        void BookOffice(BookingRequest bookingRequest);

        IEnumerable<BookingRequest> GetBookings(string locationName, string officeName);

        IEnumerable<OfficeSpecs> GetOfficeSuggestion(SuggestionRequest suggestionRequest);
    }
}