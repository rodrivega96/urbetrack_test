﻿namespace NetExam
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using NetExam.Abstractions;
    using NetExam.Dto;
    using NetExam.Info;

    public class OfficeRental : IOfficeRental
    {
        BookingInfo booking = new BookingInfo();
        LocationInfo location = new LocationInfo();
        OfficeInfo office = new OfficeInfo();

        public void AddLocation(LocationSpecs locationSpecs)
        {
            location.AddLocation(locationSpecs.Name, locationSpecs.Neighborhood);    
        }

        public void AddOffice(OfficeSpecs officeSpecs)
        {
            IEnumerable<LocationSpecs> existingLocations = location.GetCollection();
            if (existingLocations.Count() > 0)
            {
                foreach (LocationSpecs existing in existingLocations)
                {
                    if (officeSpecs.LocationName == existing.Name)
                    {
                            office.AddOffice(officeSpecs.LocationName, officeSpecs.Name, officeSpecs.MaxCapacity, officeSpecs.AvailableResources);
                            break;
                    }
                }
            } else
            {
                throw new ApplicationException("Se esta queriendo registrar una oficina en una locación inexistente.");
            }
        }

        public void BookOffice(BookingRequest bookingRequest)
        {
            IEnumerable<BookingRequest> existingBookings = booking.GetCollection();

            // Si es la primera agenda se guarda si o si.
            if(existingBookings.Count() == 0)
            {
                booking.AddBooking(bookingRequest.LocationName, bookingRequest.OfficeName, bookingRequest.DateTime, bookingRequest.Hours, bookingRequest.UserName);
            }

            foreach (BookingRequest existing in existingBookings)
            {
                if(bookingRequest.DateTime.Date == existing.DateTime.Date && 
                    Enumerable.Range(existing.DateTime.Hour, existing.DateTime.Hour + existing.Hours).Contains(bookingRequest.DateTime.Hour))
                {
                     throw new ApplicationException("No es posible agendar porque la oficina ya está ocupada.");
                } else
                {
                    booking.AddBooking(bookingRequest.LocationName, bookingRequest.OfficeName, bookingRequest.DateTime, bookingRequest.Hours, bookingRequest.UserName);
                }
            }
        }

        public IEnumerable<BookingRequest> GetBookings(string locationName, string officeName)
        {
            IEnumerable<BookingRequest> bookings = booking.GetCollection();
            if (bookings.Count() == 0) throw new ApplicationException("No hay reservas agendadas.");
            return bookings;
        }

        public IEnumerable<LocationSpecs> GetLocations()
        {
            IEnumerable<LocationSpecs> locations = location.GetCollection();
            if (locations.Count() == 0) throw new ApplicationException("No hay locaciones registradas.");
            return locations;
        }

        public IEnumerable<OfficeSpecs> GetOffices(string locationName)
        {
            IEnumerable<OfficeSpecs> offices = office.GetCollection();
            if (offices.Count() == 0) throw new ApplicationException("No hay oficinas registradas.");
            return offices;
        }

        public IEnumerable<OfficeSpecs> GetOfficeSuggestion(SuggestionRequest suggestionRequest)
        {
            // Observacion: Siempre minimamente va a tener capacidad buscada el SuggestionRequest.
            IEnumerable<OfficeSpecs> offices = office.GetCollection().OrderBy(x => x.MaxCapacity);
            List<OfficeSpecs> shownOffices = new List<OfficeSpecs>();
            var flag = false;

            // Este es el caso en el que la cantidad solicitada sea mayor que la capacidad mas grande, no hay oficinas para cubrir eso.
            if(suggestionRequest.CapacityNeeded > offices.Last().MaxCapacity)
            {
                shownOffices.Add(new OfficeSpecs("null", "null", 100000, null));
                return shownOffices;
            }

            // Para comenzar, el caso de que si existe cualquier oficina con EXACTAMENTE la cantidad necesitada, se recomienda esa, sin importar el barrio.
            foreach (OfficeSpecs offs in offices)
            {
                if(offs.MaxCapacity == suggestionRequest.CapacityNeeded &&
                    suggestionRequest.ResourcesNeeded.Intersect(offs.AvailableResources).Count()
                            == suggestionRequest.ResourcesNeeded.Count())
                {
                    shownOffices.Add(offs);
                    flag = true;
                }
            }

            if (flag) return shownOffices;

            // Aqui comienza el proceso por si cualquiera de las dos condiciones anteriores no corresponde.
            foreach (OfficeSpecs office in offices) {
                if(suggestionRequest.PreferedNeigborHood == null)
                {
                    if(suggestionRequest.ResourcesNeeded.Count() == 0)
                    {
                        if (suggestionRequest.CapacityNeeded <= office.MaxCapacity)
                        {
                            // Caso en el que solo tiene capacidad buscada y no barrio preferente ni necesidades de equipo.
                            shownOffices.Add(office);
                        }
                    }
                    else
                    {
                        // Caso en el que no tiene barrio preferente pero si recursos necesarios y capacidad buscada.
                        var equipmentExistence = suggestionRequest.ResourcesNeeded.Intersect(office.AvailableResources).Count() 
                            == suggestionRequest.ResourcesNeeded.Count();
                        if (equipmentExistence)
                        {
                            if (suggestionRequest.CapacityNeeded <= office.MaxCapacity)
                            {
                                shownOffices.Add(office);
                            }
                            return shownOffices;
                        }
                    }
                } else
                {
                    // Caso en el que se ingresa el barrio preferente. El resto es la misma logica.
                    // Todo este proceso de abajo es para obtener las oficinas que estan dentro del barrio solicitado.
                    List<OfficeSpecs> choosenOffices = new List<OfficeSpecs>();
                    IEnumerable<LocationSpecs> allLocations = location.GetCollection();
                    IEnumerable<LocationSpecs> choosenLocations = allLocations.TakeWhile(x => x.Neighborhood == suggestionRequest.PreferedNeigborHood);
                    foreach(LocationSpecs choosen in choosenLocations)
                    {
                        if(suggestionRequest.PreferedNeigborHood == choosen.Neighborhood)
                        {
                            if (suggestionRequest.ResourcesNeeded.Count() == 0)
                            {
                                if (suggestionRequest.CapacityNeeded <= office.MaxCapacity)
                                {
                                    choosenOffices.Add(office);
                                }
                            }
                            else
                            {
                                // Caso en el que no tiene barrio preferente pero si recursos necesarios y capacidad buscada.
                                var equipmentExistence = suggestionRequest.ResourcesNeeded.Intersect(office.AvailableResources).Count()
                                    == suggestionRequest.ResourcesNeeded.Count();
                                if (equipmentExistence)
                                {
                                    if (suggestionRequest.CapacityNeeded <= office.MaxCapacity)
                                    {
                                        choosenOffices.Add(office);
                                    }
                                }
                            }
                        }
                    }

                    // Si hay oficinas en el barrio solicitado que cumplan los requisitos, las devuelve a todas.
                    if(choosenOffices.Count() > 0)
                    {
                        return choosenOffices;
                    }
                    else
                    {
                        // Este camino es por si ninguna de las oficinas cuyo barrio sea el solicitado tengan oficinas con capacidad para la requerida, se recomiendan otras.
                        foreach(OfficeSpecs off in offices)
                        {
                            if (suggestionRequest.ResourcesNeeded.Count() == 0)
                            {
                                if (suggestionRequest.CapacityNeeded <= off.MaxCapacity)
                                {
                                    shownOffices.Add(off);
                                }
                            }
                            else
                            {
                                var equipmentExistence = suggestionRequest.ResourcesNeeded.Intersect(off.AvailableResources).Count()
                                    == suggestionRequest.ResourcesNeeded.Count();
                                if (equipmentExistence)
                                {
                                    if (suggestionRequest.CapacityNeeded <= off.MaxCapacity)
                                    {
                                        shownOffices.Add(off);
                                    }
                                }
                            }
                        }
                        return shownOffices;
                    }
                }
            }     
            // Si no se cumple ninguna de las condiciones anteriores, se devuelven todas las oficinas ordenadas ascendentemente.
            return offices;
        }
    }
}