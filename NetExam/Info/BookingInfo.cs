﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using NetExam.Dto;
using System.Linq;

namespace NetExam.Info
{
    public class BookingInfo
    {
        private static BookingInfo bookingInfo;

        private Dictionary<string, BookingRequest> bookingsDictionary;
        private BinaryFormatter formatter;

        private const string BOOKING_FILENAME = "bookinginformation.dat";

        // Aplicación de patrón Singleton para que solo exista una sola instancia por vez.
        public static BookingInfo Instance()
        {
            if (bookingInfo == null)
            {
                bookingInfo = new BookingInfo();
            }
            return bookingInfo;
        }

        public BookingInfo()
        {
            // Para crear diccionario para almacenar reservas en tiempo de ejecución.
            this.bookingsDictionary = new Dictionary<string, BookingRequest>();
            this.formatter = new BinaryFormatter();
        }

        public void AddBooking(string LocationName, string OfficeName, DateTime DateTime, int Hours, string UserName)
        {
            string strAux = LocationName + ", " + OfficeName + ", " + DateTime + ", " + Hours + ", " + UserName;
            BookingRequest aux = new BookingRequest(LocationName, OfficeName, DateTime, Hours, UserName);
            // Para validar que no se registre una reserva exactamente igual que otra.
            if (this.bookingsDictionary.ContainsValue(aux))
            {
                throw new ApplicationException("Dicha reserva ya existe.");
            }
            else
            {
                // Si entra aqui, corresponde agregar la reserva.
                this.bookingsDictionary.Add(strAux, aux);
                Console.WriteLine("Reserva guardada exitosamente.");
            }
        }

        // Metodo que guarda en el archivo.
        public void Guardar()
        {
            try
            {
                FileStream writerFileStream = new FileStream(BOOKING_FILENAME, FileMode.Create, FileAccess.Write);
                this.formatter.Serialize(writerFileStream, this.bookingsDictionary);
                writerFileStream.Close();
            }
            catch (Exception)
            {
                Console.WriteLine("No se puede guardar la información de las reservas.");
            }
        }

        // Metodo que carga info del archivo.
        public void Cargar()
        {
            if (File.Exists(BOOKING_FILENAME))
            {
                try
                {
                    FileStream readerFileStream = new FileStream(BOOKING_FILENAME, FileMode.Open, FileAccess.Read);
                    this.bookingsDictionary = (Dictionary<string, BookingRequest>)
                        this.formatter.Deserialize(readerFileStream);
                    readerFileStream.Close();
                }
                catch (Exception)
                {
                    Console.WriteLine("Parece que el archivo que contiene reservas existe pero existe un problema en la lectura del mismo.");
                }
            }
        }

        // Metodo que imprime el contenido del archivo.
        public void Imprimir()
        {
            if(this.bookingsDictionary.Count > 0)
            {
                Console.WriteLine("Nombre de ubicación, Nombre de oficina, Fecha de reserva, Horas de reserva, Nombre de usuario");
                foreach(BookingRequest book in this.bookingsDictionary.Values)
                {
                    Console.WriteLine(book.LocationName + ", " + book.OfficeName + ", " + book.DateTime + ", " + book.Hours + ", " + book.UserName);
                }
            } else
            {
                Console.WriteLine("No hay reservas cargadas.");
            }
        }

        // Metodo para obtener la informacion en forma de colección.
        public IEnumerable<BookingRequest> GetCollection()
        {
            List<BookingRequest> bookings = new List<BookingRequest>();
            foreach (BookingRequest booking in this.bookingsDictionary.Values)
            {
                bookings.Add(booking);
            }
            return bookings.AsEnumerable<BookingRequest>();
        }
    }
}
