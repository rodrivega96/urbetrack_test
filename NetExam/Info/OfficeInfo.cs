﻿using System.Text;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using NetExam.Dto;
using System.Collections.Generic;
using System;
using System.Linq;

namespace NetExam.Info
{
    public class OfficeInfo
    {
        private static OfficeInfo officeInfo;

        private Dictionary<string, OfficeSpecs> officesDictionary;
        private BinaryFormatter formatter;

        private const string OFFICE_FILENAME = "officeinformation.dat";

        // Aplicación de patrón Singleton para que solo exista una sola instancia por vez.
        public static OfficeInfo Instance()
        {
            if (officeInfo == null)
            {
                officeInfo = new OfficeInfo();
            }
            return officeInfo;
        }

        public OfficeInfo()
        {
            // Para crear diccionario para almacenar oficinas en tiempo de ejecución.
            this.officesDictionary = new Dictionary<string, OfficeSpecs>();
            this.formatter = new BinaryFormatter();
        }

        public void AddOffice(string LocationName, string Name, int MaxCapacity, IEnumerable<string> AvailableResources)
        {
            string strAux = LocationName + ", " + Name;
            OfficeSpecs aux = new OfficeSpecs(LocationName, Name, MaxCapacity, AvailableResources);
            // Para validar que no se registre una oficina exactamente igual que otra.
            if (this.officesDictionary.ContainsValue(aux))
            {
                throw new ApplicationException("Dicha oficina ya existe.");
            }
            else
            {
                // Si entra aqui, corresponde agregar la oficina.
                this.officesDictionary.Add(strAux, aux);
                Console.WriteLine("Oficina guardada exitosamente.");
            }
        }

        // Metodo que guarda en el archivo.
        public void Guardar()
        {
            try
            {
                FileStream writerFileStream = new FileStream(OFFICE_FILENAME, FileMode.Create, FileAccess.Write);
                this.formatter.Serialize(writerFileStream, this.officesDictionary);
                writerFileStream.Close();
            }
            catch (Exception)
            {
                Console.WriteLine("No se puede guardar la información de las oficinas.");
            }
        }

        // Metodo que carga info del archivo.
        public void Cargar()
        {
            if (File.Exists(OFFICE_FILENAME))
            {
                try
                {
                    FileStream readerFileStream = new FileStream(OFFICE_FILENAME, FileMode.Open, FileAccess.Read);
                    this.officesDictionary = (Dictionary<string, OfficeSpecs>)
                        this.formatter.Deserialize(readerFileStream);
                    readerFileStream.Close();
                }
                catch (Exception)
                {
                    Console.WriteLine("Parece que el archivo que contiene oficinas existe pero existe un problema en la lectura del mismo.");
                }
            }
        }

        // Metodo que imprime el contenido del archivo.
        public void Imprimir()
        {
            if (this.officesDictionary.Count > 0)
            {
                Console.WriteLine("Nombre de ubicación, Nombre de oficina, Capacidad máxima, Recursos disponibles");
                foreach (OfficeSpecs office in this.officesDictionary.Values)
                {
                    string resources = "";
                    foreach(string resource in office.AvailableResources)
                    {
                        resources = resources + "-" + resource;
                    }
                    Console.WriteLine(office.LocationName + ", " + office.Name + ", " + office.MaxCapacity + ", " + resources);
                }
            }
            else
            {
                Console.WriteLine("No hay oficinas cargadas.");
            }
        }

        // Metodo para obtener la informacion en forma de colección.
        public IEnumerable<OfficeSpecs> GetCollection()
        {
            List<OfficeSpecs> offices = new List<OfficeSpecs>();
            foreach (OfficeSpecs office in this.officesDictionary.Values)
            {
                offices.Add(office);
            }
            return offices.AsEnumerable<OfficeSpecs>();
        }
    }
}