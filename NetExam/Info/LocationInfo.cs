﻿using System.Text;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using NetExam.Dto;
using System.Collections.Generic;
using System;
using NetExam.Abstractions;
using System.Linq;

namespace NetExam.Info
{
    public class LocationInfo
    {
        private static LocationInfo locationInfo;

        private Dictionary<string, LocationSpecs> locationsDictionary;
        private BinaryFormatter formatter;

        private const string LOCATION_FILENAME = "locationinformation.dat";

        // Aplicación de patrón Singleton para que solo exista una sola instancia por vez.
        public static LocationInfo Instance()
        {
            if (locationInfo == null)
            {
                locationInfo = new LocationInfo();
            }
            return locationInfo;
        }

        public LocationInfo()
        {
            // Para crear diccionario para almacenar locaciones en tiempo de ejecución.
            this.locationsDictionary = new Dictionary<string, LocationSpecs>();
            this.formatter = new BinaryFormatter();
        }

        public void AddLocation(string Name, string Neighborhood)
        {
            // Para validar que no se registre una locación con el nombre de otra.
            if (this.locationsDictionary.ContainsKey(Name))
            {
                throw new ApplicationException("Dicha locación ya existe.");
            }
            else
            {
                // Si entra aqui, corresponde agregar la locación.
                this.locationsDictionary.Add(Name, new LocationSpecs(Name, Neighborhood));
                Console.WriteLine("Locación guardada exitosamente.");
            }
        }

        // Metodo que guarda en el archivo.
        public void Guardar()
        {
            try
            {
                FileStream writerFileStream = new FileStream(LOCATION_FILENAME, FileMode.Create, FileAccess.Write);
                this.formatter.Serialize(writerFileStream, this.locationsDictionary);
                writerFileStream.Close();
            }
            catch (Exception)
            {
                Console.WriteLine("No se puede guardar la información de las locaciones.");
            }
        }

        // Metodo que carga info del archivo.
        public void Cargar()
        {
            if (File.Exists(LOCATION_FILENAME))
            {
                try
                {
                    FileStream readerFileStream = new FileStream(LOCATION_FILENAME, FileMode.Open, FileAccess.Read);
                    this.locationsDictionary = (Dictionary<string, LocationSpecs>)
                        this.formatter.Deserialize(readerFileStream);
                    readerFileStream.Close();
                }
                catch (Exception)
                {
                    Console.WriteLine("Parece que el archivo que contiene locaciones existe pero existe un problema en la lectura del mismo.");
                }
            }
        }

        // Metodo que imprime el contenido del archivo.
        public void Imprimir()
        {
            if (this.locationsDictionary.Count > 0)
            {
                Console.WriteLine("Nombre de ubicación, Nombre del barrio");
                foreach (LocationSpecs location in this.locationsDictionary.Values)
                {
                    Console.WriteLine(location.Name + ", " + location.Neighborhood);
                }
            }
            else
            {
                Console.WriteLine("No hay locaciones cargadas.");
            }
        }

        // Metodo para obtener la informacion en forma de colección.
        public IEnumerable<LocationSpecs> GetCollection()
        {
            List<LocationSpecs> locations = new List<LocationSpecs>();
            foreach(LocationSpecs location in this.locationsDictionary.Values)
            {
                locations.Add(location);
            }
            return locations.AsEnumerable<LocationSpecs>();
        }
    }
}